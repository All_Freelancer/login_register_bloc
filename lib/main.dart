import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/material.dart';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';

import 'package:login_register_bloc_app/src/ui/pages/splash.dart';
import 'package:login_register_bloc_app/src/util/const_global.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'src/bloc/user_bloc.dart';
import 'src/util/LanguageLocalizations.dart';

//******************************************************** CLASS MAIN ********************************************** */
void main() async{

  SharedPreferences prefs = await SharedPreferences.getInstance();
  ConstGlobal.token = prefs.getString('token');
  
  runApp(MyApp());
}
//********************************************************* CLASS MY APP ********************************************* */
class MyApp extends StatefulWidget {
 
  //************************* CALL STATE ********************* */
  @override
  _MyAppState createState() => _MyAppState();
}

//***************************************************** STATE MY APP ********************************************* */
class _MyAppState extends State<MyApp> {
  
  FirebaseAnalytics analytics = FirebaseAnalytics();

  
  //*********************** This widget is the root of your application. ***********************
  @override
  Widget build(BuildContext context) {
/*
    return UserBlocProvider( 
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',

        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          //primarySwatch: Colors.blue,
          //accentColor: Colors.black,

          primaryColor: Colors.black,
          accentColor: Colors.black,
          primarySwatch: Colors.blue,
          
        ),

        home: Scaffold(
            /* 
            appBar: AppBar(
              title: Text(
                "Goals",
                style: TextStyle(color: Colors.black),
              ),
              backgroundColor: Colors.amber,
              elevation: 0.0,
            ),
            */
            body: SplashPage(),
        )  
      )  
    );
*/
    return BlocProvider(
      blocs: [Bloc((i) => UserBloc())],

      child: MaterialApp(
        //::::::::::::::::::::::::::::: LANGUAGE :::::::::::::::::::::::::::
        supportedLocales: [
          const Locale('en', 'US'),           //FIRST 
          const Locale('pt', 'PT')            //SECOND
        ],
        localizationsDelegates: [
          const LanguageDelegate(),
          GlobalCupertinoLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate
        ],

        localeResolutionCallback:(Locale locale, Iterable<Locale> supportedLocales) {
          if (locale == null) {
            //  debugPrint("*language locale is null!!!");
            return supportedLocales.first;
          }

          for (Locale supportedLocale in supportedLocales) {
            if (supportedLocale.languageCode == locale.languageCode ||
              supportedLocale.countryCode == locale.countryCode) {
              //  debugPrint("*language ok $supportedLocale");
              return supportedLocale;
            }
          }

          // debugPrint("*language to fallback ${supportedLocales.first}");
          return supportedLocales.first;
        },

        //:::::::::::::::::::::::::::::: THEME :::::::::::::::::::::::::::::::
        theme: ThemeData(
          
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          //primarySwatch: Colors.blue,
          //accentColor: Colors.black,

          primaryColor: Colors.black,
          accentColor: Colors.black,
          primarySwatch: Colors.blue,
            
        ),

        //::::::::::::::::::::::::::::::: START HOME :::::::::::::::::::::::::
        home: Scaffold(
            /* 
            appBar: AppBar(
              title: Text(
                "Goals",
                style: TextStyle(color: Colors.black),
              ),
              backgroundColor: Colors.amber,
              elevation: 0.0,
            ),
            */
            body: SplashPage(),
        ),  

        //:::::::::::::::::::::::::::::: FIREBASE ANALYTIC :::::::::::::::::::::
        navigatorObservers: [
              FirebaseAnalyticsObserver(analytics: analytics),
            ],

      )
      
    );  
  }
}
//********************************************************************************************************************* */