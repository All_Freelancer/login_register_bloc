import 'dart:async';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:login_register_bloc_app/src/repository/auth.dart';
import 'package:login_register_bloc_app/src/util/LanguageLocalizations.dart';
import 'package:login_register_bloc_app/src/util/validators.dart';

import 'package:rxdart/rxdart.dart';

//************************************** CLASS LOGIN BLOC ************************************************ */
class UserBloc extends BlocBase with Validators{
  //final _repository = UserRepository();

  final _displayName = "Kiffen"; 

  final _email = BehaviorSubject<String>();
  final _password = BehaviorSubject<String>();
  final _confirmPassword = BehaviorSubject<String>();
  final _isSignedIn = BehaviorSubject<bool>();

  //**************** RxDart is a reactive functional programming library for Google Dart *******************/
  //:::::::::::::::::: STRING
  String get displayName => _displayName; 
  String get emailAddress => _email.value;
  String get passwordCode => _password.value; 
  


  //:::::::::::::::::: OBSERVABLE 
  Observable<String> get email => _email.stream.transform(validateEmail);

  Observable<String> get password =>_password.stream.transform(validatePassword);
  Observable<String> get confirmPassword =>_confirmPassword.stream.transform(validatePassword);

  Observable<bool> get signInStatus => _isSignedIn.stream;

  //::::::::::::::::::: Change data 
  Function(String) get changeEmail => _email.sink.add;

  Function(String) get changePassword => _password.sink.add;

  Function(String) get changeConfirmPassword => _confirmPassword.sink.add; 
 
  Function(bool) get showProgressBar => _isSignedIn.sink.add;

  //************************************** CALL REPOSITORY *********************************************** */
  //:::::::::::::: AUTHENTICATE 
  Future<String> loginUser() {
    return AuthRepository.signIn(_email.value, _password.value);
  }

  //:::::::::::::: REGISTER USER
  Future<String> registerUser() {
    return AuthRepository.signUp(_email.value, _password.value);
  }

  //****************************************** METHOD VALIDATE FIELD ***************************************** */
  //:::::::::::::: LOGIN 
  String validateFieldsLogin(BuildContext context) {  
    
    if (_email.value != null &&
        _email.value.isNotEmpty &&
        _password.value != null &&
        _password.value.isNotEmpty &&
        _email.value.contains('@') &&
        _password.value.length >= 6) {


      return "true"; 
    
    } else {
      return Language.of(context).trans('error_message');
    }
  }

  //:::::::::::::: REGISTER 
  String validateFieldsRegister(BuildContext context) {  

    if (_email.value != null &&
        _email.value.isNotEmpty &&
        _password.value != null &&
        _password.value.isNotEmpty &&
        _email.value.contains('@') &&
        _password.value.length >= 6) {

      if(_password == _confirmPassword )
        return "true"; 
    
      return Language.of(context).trans('equalPassword_validateMessage');
    } else {
      return Language.of(context).trans('error_message');
    }
  }

  //******************************************** METHOD DISPOSE ******************************************** */
  @override
  void dispose() async{
    super.dispose();  
    //await _email.drain(); 
    _email.close(); 

    //await _password.drain();
    _password.close();

    //await _confirmPassword.drain();
    _confirmPassword.close(); 

    //await _isSignedIn.drain();
    _isSignedIn.close();
  }
}
//********************************************************************************************************** */