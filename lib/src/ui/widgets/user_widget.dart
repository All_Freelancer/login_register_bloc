
import 'package:flutter/material.dart';
import 'package:login_register_bloc_app/src/bloc/user_bloc.dart';
import 'package:login_register_bloc_app/src/model/user.dart';
import 'package:login_register_bloc_app/src/repository/auth.dart';
import 'package:login_register_bloc_app/src/ui/pages/home.dart';

//******************************************************** CLASS BUILD WIDGETS ************************************************** */
class UserWidgets{

  //************************************* WIDGET TEXT ************************************************ */
  //::::::::::::::::::::: TEXT ROW 
  static Widget buildTextRow(String title){
    return Row(
      children: <Widget>[
        new Expanded(
          child: new Padding(
            padding: const EdgeInsets.only(left: 40.0),
            child: new Text(
              title,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.redAccent,
                fontSize: 15.0,
              ),
            ),
          ),
        ),
      ],
    ); 
  }
  
  //:::::::::::::::::::::: TEXT
  static Widget buildText(String title, {TextAlign textAlign,  double sizetext, Color colorText, FontWeight fontWeight}){
    return Text(
      title,
      textAlign: textAlign??TextAlign.center,
      style: TextStyle(
        
        color: colorText??Colors.white,                   //if color is null defaut white
        fontSize: sizetext??20.0,
        fontWeight: fontWeight??FontWeight.normal,
      )  
   ); 
  }

  //******************************** WIDGET TEXT FIELD *********************************** */
  static Widget buildTextField(BuildContext context, UserBloc blocUser, String nameField, String hint){
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.only(left: 40.0, right: 40.0, top: 10.0),
      alignment: Alignment.center,

      //::::::::::::::::::::::::::::: ERROR LINE :::::::::::::::
      /*
      decoration: BoxDecoration(
        border: Border(
        bottom: BorderSide(
          color: Colors.redAccent,
          width: 1.5,
          style: BorderStyle.solid),
        ),
      ),*/
      
      padding: const EdgeInsets.only(left: 0.0, right: 10.0),

      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          new Expanded(
            child: getCustomTextField(blocUser, nameField, hint),
           
          ),
        ],
      ),
    ); 
  }

  //:::::::::::::: CHOICE 
  static Widget getCustomTextField(bloc, String nameField, String hint){

    switch (nameField) {
      case "email":
          return emailField(bloc, hint); 
        break;
      case "password":
          return passwordField(bloc, hint);
        break;

      default:
        return confirmPasswordField(bloc, hint); 
    }
  }

  //::::::::::: EMAIL FIELD 
  static Widget emailField(bloc, String hint) {
    return StreamBuilder(
      stream: bloc.email,
      builder: (context, snapshot) {
        return TextField(
          onChanged: bloc.changeEmail,
          keyboardType: TextInputType.emailAddress,

          decoration: InputDecoration(
            //border: OutlineInputBorder(),
            hintText: hint,
             //labelText: 'Email Address',
            hintStyle: TextStyle(color: Colors.grey),
            errorText: snapshot.error,
          ),
        );
      },
    );  
  }

  //:::::::::::: PASSWORD FIELD 
  static Widget passwordField(bloc, String hint) {
    return StreamBuilder(
      stream: bloc.password,
      builder: (context, snapshot) {
        return TextField(
          obscureText: true,
          onChanged: bloc.changePassword,

          decoration: InputDecoration(
            //hintText: 'Must contain 8 characters',
            hintText: hint,
            //labelText: 'Password',
            hintStyle: TextStyle(color: Colors.grey),
            errorText: snapshot.error
          ),

        );
      }, 
    );
  }
  
  //:::::::::::::: CONFIRM PASSWORD FIELD
  static Widget confirmPasswordField(bloc, String hint) {
    return StreamBuilder(
      stream: bloc.confirmPassword,
      builder: (context, snapshot) {
        return TextField(
          obscureText: true,
          onChanged: bloc.changeConfirmPassword,

          decoration: InputDecoration(
            //hintText: 'Must contain 8 characters',
            hintText: hint,
            //labelText: 'Password',
            hintStyle: TextStyle(color: Colors.grey),
            errorText: snapshot.error
          ),

        );
      }, 
    );
  }
  
  //************************************* WIDGET BUTTON ********************************************** */
  //::::::::::::::::::::::::::::: OUTLINEBUTTON 
  static Widget buildOutLineButton(BuildContext context, String text, Function pressed, Color colorButton, Color colorBorder){

    return Container(
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.only(left: 30.0, right: 30.0, top: 150.0),
      alignment: Alignment.center,

      child: new Row(
        children: <Widget>[
          new Expanded(
            child: new OutlineButton(
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)
               ),
                      
              color: colorButton,                             //Colors.redAccent,
              //highlightedBorderColor: Colors.black,
              borderSide: BorderSide(color: colorBorder),     //Colors.white 

              onPressed: pressed,                             // CALL FUNCTION gotoSignup(),
                      
              child: new Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 20.0,
                  horizontal: 20.0,
                ),

                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[

                    buildText(text, sizetext: 14.0, fontWeight: FontWeight.bold), 
                
                  ],
                )  
              ),

            ),
          ),
        ],
      ),
    ); 
  }

  //:::::::::::::::::::::::::::::: FLATBUTTON
  static Widget buildFlatButton(BuildContext context, String text, Function pressed, Color colorButton,
                                Color colortText, double topMargin){

    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(left: 30.0, right: 30.0, top: topMargin),
      alignment: Alignment.center,

      //---------------------------- ROW ------------------------
      child: new Row(  
        children: <Widget>[
          new Expanded(
            child: new FlatButton(
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)
              ),
                      
              color: colorButton,                                     //COLOR BUTTON   
              onPressed: pressed,                                     //CALL FUNCTION
              //onPressed: () => pressed(context),                    //CALL FUNCTION ANONYMOUS 
              //onPressed: bloc.submit 

              child: new Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 20.0,
                  horizontal: 20.0,
                ),
                
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    buildText(text, sizetext: 14.0, colorText: colortText, fontWeight: FontWeight.bold), 
                    
                  ],
                ),

              ),
            ),
          ),
        ],
      ),
    );  
  } 

  //:::::::::::::::::::::::::::::: LOGIN BUTTON
  static Widget submitLogin(BuildContext context, UserBloc bloc, String text){
    
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(left: 30.0, right: 30.0, top: 40.0),
      alignment: Alignment.center,

      //---------------------------- ROW ------------------------
      child: new Row(  
        children: <Widget>[
          new Expanded(
   
            child: StreamBuilder(
              //stream: bloc.submitValid,
              builder: (context, snapshot){
                return new FlatButton(
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0)
                  ),
                      
                  color: Colors.redAccent,                                       //COLOR BUTTON   
                  //onPressed: () => pressed(context),                           //CALL FUNCTION ANONYMOUS 
                  //onPressed: bloc.submit 
                  //onPressed: bloc.registerUser,                                //CALL FUNCTION

                  onPressed: () {
                    String validateField = bloc.validateFieldsLogin(context);  
                    if (validateField=="true") {                                //VALIDATION FIELD
                      _authenticateUser(context, bloc);
                    } else {
                      showErrorMessage(context, validateField);
                    }
                  },

                  child: new Container(
                    padding: const EdgeInsets.symmetric(
                      vertical: 20.0,
                      horizontal: 20.0,
                    ),
                
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        buildText(text, sizetext: 14.0, colorText: Colors.white, fontWeight: FontWeight.bold), 
                    
                      ],
                    ),

                  ),
                );
              }
            ),
            
          ),
        ],
      ),
    );
  }

  //:::::::::::::::::::::::::::::: REGISTER BUTTON 
  static Widget submitRegister(BuildContext context, UserBloc bloc, String text) {

    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(left: 30.0, right: 30.0, top: 40.0),
      alignment: Alignment.center,

      //---------------------------- ROW ------------------------
      child: new Row(  
        children: <Widget>[
          new Expanded(
   
            child: StreamBuilder(
              //stream: bloc.submitValid,
              builder: (context, snapshot){
                return new FlatButton(
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0)
                  ),
                      
                  color: Colors.redAccent,                                       //COLOR BUTTON   
                  //onPressed: () => pressed(context),                           //CALL FUNCTION ANONYMOUS 
                  //onPressed: bloc.submit 
                  //onPressed: bloc.registerUser,                                //CALL FUNCTION

                  onPressed: () {
                    String validateField = bloc.validateFieldsRegister(context);  
                    if (validateField=="true") {                                //VALIDATION FIELD
                      _registerUser(context, bloc);
                    } else {
                      showErrorMessage(context, validateField);
                    }
                  },

                  child: new Container(
                    padding: const EdgeInsets.symmetric(
                      vertical: 20.0,
                      horizontal: 20.0,
                    ),
                
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        buildText(text, sizetext: 14.0, colorText: Colors.white, fontWeight: FontWeight.bold), 
                    
                      ],
                    ),

                  ),
                );
              }
            ),
            
          ),
        ],
      ),
    );
  }

  //************************************ METHOD AUTHENTICATE ********************************************* */
  //::::::::::::::::::::::::::: REQUEST LOGIN 
  static void _authenticateUser(BuildContext context, UserBloc bloc) {
    
    //bloc.showProgressBar(true); 

    showDialog(
          context: context,
          builder: (BuildContext context) {
              return Center(child: CircularProgressIndicator(),);
          });

    bloc.loginUser().then((onValue) async {
      if(onValue =='login'){
        /*
        await loginAction().then((onValue){
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => HomePage(),
            )
          ); 
        });
        */
        new Future.delayed(new Duration(seconds: 2), () {
          //Navigator.pop(context); //pop dialog
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => HomePage(),
            )
          ); 
        });
      
      }else{
        showErrorMessage(context, onValue); 
      }
    }); 
  }

  static Future<bool> loginAction() async {
    //replace the below line of code with your login request
    await new Future.delayed(const Duration(seconds: 2));
    return true;
  }

  //::::::::::::::::::::::::::: REQUEST REGISTER 
  static void _registerUser(BuildContext context, UserBloc bloc) {
    
    bloc.registerUser().then((onValue) {
      if (onValue == "register") {

         bloc.showProgressBar(true);
        //New User
        UserModel user = UserModel(userID: onValue, firstName: bloc.displayName, email:bloc.emailAddress); 
        AuthRepository.addUser(user); 
        
      } else {
        //Already registered --- Show Dialog
        /*
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => HomePage(),
          )
        );
        */
        showErrorMessage(context, onValue); 
      }
    });  
  }

  //*********************** METHOD SHOW ERROR ************************* */
  static void showErrorMessage(BuildContext context, String errorMessage) {
    final snackbar = SnackBar(
      content: Text(errorMessage),
      duration: new Duration(seconds: 2)
    );
    
    Scaffold.of(context).showSnackBar(snackbar);
  }

}
//************************************************************************************************************************************ */