import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:login_register_bloc_app/src/bloc/user_bloc.dart';
import 'package:login_register_bloc_app/src/util/LanguageLocalizations.dart';

//********************************************* CLASS SIGN IN FORM *********************************** */
class SignInForm extends StatefulWidget {
  
  //************************ CALL STATE ****************** */
  @override
  SignInFormState createState() {
    return SignInFormState();
  }
}

//****************************************** STATE SIGN IN FORM *************************************** */
class SignInFormState extends State<SignInForm> {
  //Variable
  UserBloc _bloc;

   //********************** DEPENDENCIES ******************** */
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    //_bloc = UserBlocProvider.of(context);

    _bloc = BlocProvider.getBloc<UserBloc>();
  }

  //************************* DISPOSE ************************ */
  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  //*********************** WIDGET ROOT ********************** */
  @override
  Widget build(BuildContext context) {

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        emailField(),  
        Container(margin: EdgeInsets.only(top: 5.0, bottom: 5.0)),
        passwordField(),
        Container(margin: EdgeInsets.only(top: 5.0, bottom: 5.0)),
        submitButton(context)
      ],
    );
  }

  //*********************** WIDGET PASSWORD ************************* */
  Widget passwordField() {
    return StreamBuilder(
        stream: _bloc.password,
        builder: (context, AsyncSnapshot<String> snapshot) {
          return TextField(
            onChanged: _bloc.changePassword,
            obscureText: true,
            decoration: InputDecoration(
                hintText: Language.of(context).trans('password_hint'),
                errorText: snapshot.error
            ),
          );
        });
  }

 //*********************** WIDGET EMAIL ********************************* */
  Widget emailField() {
    return StreamBuilder(
        stream: _bloc.email,
        builder: (context, snapshot) {
          return TextField(
            onChanged: _bloc.changeEmail,
            decoration: InputDecoration(
                hintText: Language.of(context).trans('email_hint'), 
                errorText: snapshot.error
            ),
          );
        });
  }

 //*********************** WIDGET PASS SUBMIT PASSWORD ************************* */
  Widget submitButton(BuildContext context) {
    return StreamBuilder(
        stream: _bloc.signInStatus,
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (!snapshot.hasData || snapshot.hasError) {
            return button(context);
          } else {
            return CircularProgressIndicator();       //LOADING ....
          }
        });
  }

 //**************************** WIDGET BUTTON ***************************** */
  Widget button(BuildContext context) {
    return RaisedButton(
        child: Text(Language.of(context).trans('submit')),
        textColor: Colors.white,
        color: Colors.black,

        shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0)),

        onPressed: () {
          if (_bloc.validateFieldsRegister(context)=="true") {
            authenticateUser();
          } else {
            showErrorMessage();
          }
        });
  }

  //*********************** METHOD AUTHENTICATE ************************* */
  void authenticateUser() {
    _bloc.showProgressBar(true);
    /*
    _bloc.submit().then((value) {
      if (value == 0) {
        //New User
        _bloc.registerUser().then((value) {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => GoalsList(_bloc.emailAddress)));
        });
      } else {
        //Already registered
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => GoalsList(_bloc.emailAddress)));
      }
   
    })
    */  
  }

  //*********************** METHOD SHOW ERROR ************************* */
  void showErrorMessage() {
    final snackbar = SnackBar(
      content: Text(Language.of(context).trans('error_message')),
      duration: new Duration(seconds: 2)
    );
    
    Scaffold.of(context).showSnackBar(snackbar);
  }
}
//************************************************************************************************************** */