import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'package:login_register_bloc_app/src/bloc/user_bloc.dart';
import 'package:login_register_bloc_app/src/ui/pages/home.dart';
import 'package:login_register_bloc_app/src/ui/pages/splash.dart';
import 'package:login_register_bloc_app/src/ui/widgets/user_widget.dart';

import 'package:login_register_bloc_app/src/util/LanguageLocalizations.dart';

//************************************************ CLASS LOGIN PAGE ************************************** */
class RegisterPage extends StatelessWidget {
  //Variable 
  //UserBloc blocUser; 

  //*********************** CONSTRUCT *********************************
   RegisterPage({Key key}) : super(key: key);
  //RegisterPage({Key key}) : super(key: key);

  //*********************** WIDGET PASSWORD ************************* */
  @override
  Widget build(BuildContext context) {

    //final blocUser = UserBlocProvider.of(context);
    final blocUser =  BlocProvider.getBloc<UserBloc>();

    return signupPage(context, blocUser);
  }
  
  //******************************************* WIDGET SIGUP - REGISTER ******************************** */
  Widget signupPage(BuildContext context, blocUser) {
    String emailText = Language.of(context).trans('email').toUpperCase(); 
    String passwordText = Language.of(context).trans('password').toUpperCase(); 
    String confirmPasswordText = Language.of(context).trans('confirm_password').toUpperCase(); 

    return new Container(
      height: MediaQuery.of(context).size.height,
      //:::::::::::::::::::::::::::::::::: BACKGROUND :::::::::::::::::::::::::::::::
      decoration: BoxDecoration(
        color: Colors.white,
        image: DecorationImage(
          colorFilter: new ColorFilter.mode(
              Colors.black.withOpacity(0.05), BlendMode.dstATop),
          image: AssetImage('assets/images/mountains.jpg'),
          fit: BoxFit.cover,
        ),
      ),

      //::::::::::::::::::::::::::::: SINGLE CHIL SCROLL VIEW :::::::::::::::::::::::
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 30),
        child: SingleChildScrollView(

          //:::::::::::::::::::::::::::::::: COLUMN :::::::::::::::::::::::::::::::::
          child: new Column(
            children: <Widget>[
              //--------------------------- LOGO APP --------------------------
              Container(
                padding: EdgeInsets.all(100.0),
                child: Center(
                  child: Icon(
                    Icons.headset_mic,
                    color: Colors.redAccent,
                    size: 50.0,
                  ),
                ),
              ),

              //--------------------------- TEXT EMAIL ------------------------
              UserWidgets.buildTextRow(emailText),
          
              //------------------------ TEXT FIELD EMAIL ---------------------
              UserWidgets.buildTextField(
                context, 
                blocUser, 
                Language.of(context).trans('email'), 
                //Language.of(context).trans('email_hint')),
                "samarthagarwal@live.com"), 

              //----------------------------- SEPARATE ------------------------
              Divider(
                height: 24.0,
              ),

              //--------------------------- TEXT PASSWORD ---------------------
              UserWidgets.buildTextRow(passwordText),
              
              //------------------------ TEXT FIELD PSSWORD -------------------
              UserWidgets.buildTextField(
                context, 
                blocUser, 
                Language.of(context).trans('password'),
                //Language.of(context).trans('password_hint')),
                "******"),
            
              //----------------------------- SEPARATE ------------------------
              Divider(
                height: 24.0,
              ),

              //--------------------------- TEXT PASSWORD ---------------------
              UserWidgets.buildTextRow(confirmPasswordText),
              
              //------------------------ TEXT FIELD PSSWORD -------------------
              UserWidgets.buildTextField(
                context, 
                blocUser, 
                Language.of(context).trans('confirmPassword'), 
                //Language.of(context).trans('confirmPassword_hint')),
                "******"),

              //----------------------------- SEPARATE ------------------------
              Divider(
                height: 24.0,
              ),

              //-------------------------- I HAVE AN ACCOUNT ------------------
              new Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 20.0),
                    child: new FlatButton(

                      child: UserWidgets.buildText(
                        Language.of(context).trans('have_account'), 
                        fontWeight: FontWeight.bold,
                        sizetext: 15.0, 
                        colorText: Colors.redAccent, 
                        textAlign: TextAlign.end), 

                      //----------- Click -------
                      onPressed: () => SplashPage.gotoLogin(),

                    ),
                  ),
                ],
              ),

              //--------------------------- BUTTON REGISTER --------------------
              //BuildWidgets.buildFlatButton(context, "SIGN UP", register, 
              //                             Colors.redAccent, Colors.white, 40.0, blocUser: blocUser),
              UserWidgets.submitRegister(context, blocUser, Language.of(context).trans('sign_up').toUpperCase()),
            
            ],
          ),

        )
      )  
    );
  }

  //************************************* FUCTION REGISTER ******************************* */
  void register(BuildContext context, UserBloc blocUser) async{
    //final FormState form = _formKey.currentState;
    //final FormState form  = ConstGlobal.formKey.currentState; 

    //if (ConstGlobal.formKey.currentState.validate()) {
    //  form.save();
      
      try {
        FirebaseUser user = (await FirebaseAuth.instance
            .createUserWithEmailAndPassword(
            email: blocUser.emailAddress, password: blocUser.passwordCode)).user;

        UserUpdateInfo userUpdateInfo = new UserUpdateInfo();

        userUpdateInfo.displayName = blocUser.displayName;
        user.updateProfile(userUpdateInfo).then((onValue) {
          //Navigator.of(context).pushReplacementNamed('/home');
          Navigator.push(context, MaterialPageRoute(
              builder: (context) => HomePage() ));
          
          Firestore.instance.collection('users').document().setData(
              {'email': blocUser.emailAddress, 'displayName': blocUser.displayName/*});.then((onValue) {
            _sheetController.setState(() {
              _loading = false;
            });*/
          });
        });
      } catch (error) {
        print(error.toString()); 
      }
    }  
  //}
}
//********************************************************************************************************** */