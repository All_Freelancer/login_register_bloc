import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:login_register_bloc_app/src/bloc/user_bloc.dart';
import 'package:login_register_bloc_app/src/ui/pages/home.dart';
import 'package:login_register_bloc_app/src/ui/pages/login.dart';
import 'package:login_register_bloc_app/src/ui/pages/register.dart';
import 'package:login_register_bloc_app/src/ui/widgets/loading.dart';
import 'package:login_register_bloc_app/src/ui/widgets/user_widget.dart';

import 'package:login_register_bloc_app/src/util/LanguageLocalizations.dart';
import 'package:login_register_bloc_app/src/util/const_global.dart';
import 'package:login_register_bloc_app/src/util/validators.dart';

//***************************************************** CLASS SPLASH ************************************************* */
class SplashPage extends StatefulWidget {
  //Variable
  static PageController _controller = new PageController(initialPage: 1, viewportFraction: 1.0);

  //************************************* CONSTRUCT ************************************ */
  SplashPage({Key key}) : super(key: key);

  //********************************************* METHOD GO TO LOGIN ********************************* */  
  static gotoLogin() {
    //controller_0To1.forward(from: 0.0);
    _controller.animateToPage(
      2,
      duration: Duration(milliseconds: 800),
      curve: Curves.bounceOut,
    );
  }

  //******************************************** METHOD GO TO SIGNUP ********************************* */  
  static gotoSignup() {
    //controller_minus1To0.reverse(from: 0.0);
    _controller.animateToPage(
      0,
      duration: Duration(milliseconds: 800),
      curve: Curves.bounceOut,
    );
  }

  //************************************* CALL STATE ************************************ */
  @override
  _SplashPageState createState() => _SplashPageState();
}

//***************************************************** STATE SPLSH ************************************************** */
class _SplashPageState extends State<SplashPage> with TickerProviderStateMixin {
  //variable state
  double scrollPercent = 0.0;
  Offset startDrag;
  double startDragPercentScroll;
  double dragDirection; // -1 for left, +1 for right

  AnimationController controllerMinus1To0;
  AnimationController controller_0To1;
  CurvedAnimation animMinus1To0;
  CurvedAnimation anim_0To1;

  final numCards = 3;
  final blocUser =  BlocProvider.getBloc<UserBloc>();

  //********************************************* INIT STATE ***************************************** */
  @override
  void initState() {
    super.initState();
     
    //The code is commented because instead of manual scrolling with animation,
    //Now PageView is being used
    controllerMinus1To0 = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
      lowerBound: -1.0,
      upperBound: 0.0,
    );
    controller_0To1 = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
      lowerBound: 0.0,
      upperBound: 1.0,
    );

    animMinus1To0 = new CurvedAnimation(
      parent: controllerMinus1To0,
      curve: Interval(0.10, 0.90, curve: Curves.bounceInOut),
    );
    anim_0To1 = new CurvedAnimation(
      parent: controller_0To1,
      curve: Interval(0.10, 0.90, curve: Curves.bounceInOut),
    );

    anim_0To1.addListener(() {
      scrollPercent = controller_0To1.value / numCards;
      //print(scrollPercent);
      setState(() {});
    });

    animMinus1To0.addListener(() {
      scrollPercent = controllerMinus1To0.value / numCards;
      //print(scrollPercent);
      setState(() {});
    });
    
  }

  //*********************************************** WIDGET ROOT ************************************** */
  @override
  Widget build(BuildContext context) {
    //SystemChrome.setEnabledSystemUIOverlays([]);                  //FULL SCREEN
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(      //STATUS BAR COLOR
      statusBarColor: Colors.transparent
    ));

    Validators.contextValidator = context;      //INIT CONTEXT BECAUSE TRANSLATER (LANGUAGE)
    /*
    return StreamBuilder(
      stream: blocUser.signInStatus,            //when have change waiting......
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        if (!snapshot.hasData || snapshot.hasError) {
          return loginOrRegister();
        } else {
          blocUser.showProgressBar(false); 
          //return CircularProgressIndicator();       //LOADING ...
          return homePage(true); 
          
          /*
          Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => HomePage(),
          )
          */
        }
    });*/

    Widget splashHome; 

    if (ConstGlobal.token != null && ConstGlobal.token != "")
      splashHome = homePage(true); 
    else
      splashHome = loginOrRegister();
  
    return Material(
      
      child: splashHome
    );
  }

  //*********************************** WIDGET LOGIN OR REGISTER ************************************* */
  Widget loginOrRegister(){

    return Container(
      height: MediaQuery.of(context).size.height,
      child: new GestureDetector(
        onHorizontalDragStart: _onHorizontalDragStart,
        onHorizontalDragUpdate: _onHorizontalDragUpdate,
        onHorizontalDragEnd: _onHorizontalDragEnd,
        behavior: HitTestBehavior.translucent,
        /*
        child: Stack(
          children: <Widget>[
            new FractionalTranslation(
              translation: Offset(-1 - (scrollPercent / (1 / numCards)), 0.0),
              child: signupPage(context),
            ),
            
            new FractionalTranslation(
              translation: Offset(0 - (scrollPercent / (1 / numCards)), 0.0),
              child: homePage(),
            ),
              
            new FractionalTranslation(
              translation: Offset(1 - (scrollPercent / (1 / numCards)), 0.0),
               child: loginPage(context),
            ),
          
          ],
        ),
        */
        child: PageView(
          controller: SplashPage._controller,
          physics: new AlwaysScrollableScrollPhysics(),
          children: <Widget>[
            RegisterPage(),                   //1º page
            homePage(false),                  //2º page
            LoginPage(),                      //3º page
           
          ],
          scrollDirection: Axis.horizontal,
        )  
    ));
  }

  //****************************************** WIDGET HOME PAGE ************************************** */
  Widget homePage(bool loading) {

    return new Container(
      height: MediaQuery.of(context).size.height,

      //:::::::::::::::::::::::::::::::::: BACKGROUND :::::::::::::::::::::::::::::::
      decoration: BoxDecoration(
        color: Colors.redAccent,
        image: DecorationImage(
          colorFilter: new ColorFilter.mode(
              Colors.black.withOpacity(0.3), BlendMode.dstATop),
          image: AssetImage('assets/images/mountains.jpg'),
          fit: BoxFit.cover,
        ),
      ),

      //::::::::::::::::::::::::::::: SINGLE CHIL SCROLL VIEW :::::::::::::::::::::::
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 30),
        child: SingleChildScrollView(

          //::::::::::::::::::::::::::::::: COLUMN ::::::::::::::::::::::::::::::::::
          child: new Column(
            children: <Widget>[
              //------------------------- ICON HEADSET -------------------
              iconHeadSet(),
            
              //------------------------- NAME APP ------------------------
              Container(
                padding: EdgeInsets.only(top: 20.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    UserWidgets.buildText("Awesome"), 
                    UserWidgets.buildText("App", fontWeight:FontWeight.bold), 
                  ],
                ),
              ),

              nextStep(loading),

              //------------------------- SIGN UP -------------------------
              //UserWidgets.buildOutLineButton(context, signUp, SplashPage.gotoSignup, Colors.redAccent, Colors.white), 

              //-------------------------- LOGIN --------------------------
              //UserWidgets.buildFlatButton(context, login, SplashPage.gotoLogin, Colors.white, Colors.redAccent, 30.0),
            
            ],
          ),
        )
      )    
    );
  }

  //:::::::::::::::::::::::::::: NEXT STEP 
  Widget nextStep(bool loading){
    String signUp = Language.of(context).trans('register').toUpperCase(); 
    String login = Language.of(context).trans('login').toUpperCase();

    if(loading){
         
         Timer.periodic(Duration(seconds: 2), (t){
           print(t.tick); 

         if(t.tick>=2){
           t.cancel();
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => HomePage(),
              )
            );
         } 
       });
        
      return Container(
        width: MediaQuery.of(context).size.width,
        margin: const EdgeInsets.only(left: 30.0, right: 30.0, top: 150.0),
        alignment: Alignment.center,

        child: ColorLoader()
      );
     
    }else{
      return Column(
        children: <Widget>[

          //------------------------- SIGN UP -------------------------
          UserWidgets.buildOutLineButton(context, signUp, SplashPage.gotoSignup, Colors.redAccent, Colors.white), 
          
          //-------------------------- LOGIN --------------------------
          UserWidgets.buildFlatButton(context, login, SplashPage.gotoLogin, Colors.white, Colors.redAccent, 30.0),
          
        ],
      );
    }
  }

  //*********************************** WIDGET ICON HEADSET ****************************************** */
  Widget iconHeadSet(){
    return Container(
      padding: EdgeInsets.only(top: 250.0),
      child: Center(
        child: Icon(
            Icons.headset_mic,
            color: Colors.white,
            size: 40.0,
        ),
      ),
    ); 
  }

  //**************************************** DRAG START (PRIVATE) ************************************ */
  void _onHorizontalDragStart(DragStartDetails details) {
    startDrag = details.globalPosition;
    startDragPercentScroll = scrollPercent;
  }

  //*****************************************  DRAG UPDATE (PRIVATE) ********************************* */
  void _onHorizontalDragUpdate(DragUpdateDetails details) {
    final currDrag = details.globalPosition;
    final dragDistance = currDrag.dx - startDrag.dx;
    if (dragDistance > 0) {
      dragDirection = 1.0;
    } else {
      dragDirection = -1.0;
    }
    final singleCardDragPercent = dragDistance / context.size.width;

    setState(() {
      scrollPercent =
          (startDragPercentScroll + (-singleCardDragPercent / numCards))
              .clamp(0.0 - (1 / numCards), (1 / numCards));
      print(scrollPercent);
    });
  }

  //******************************************  DRAG END (PRIVATE) *********************************** */
   void _onHorizontalDragEnd(DragEndDetails details) {
    if (scrollPercent > 0.1666) {
      print("FIRST CASE");
      controller_0To1.forward(from: scrollPercent * numCards);
    } else if (scrollPercent < 0.1666 &&
        scrollPercent > -0.1666 &&
        dragDirection == -1.0) {
      print("SECOND CASE");
      controller_0To1.reverse(from: scrollPercent * numCards);
    } else if (scrollPercent < 0.1666 &&
        scrollPercent > -0.1666 &&
        dragDirection == 1.0) {
      print("THIRD CASE");
      controllerMinus1To0.forward(from: scrollPercent * numCards);
    } else if (scrollPercent < -0.1666) {
      print("LAST CASE");
      controllerMinus1To0.reverse(from: scrollPercent * numCards);
    }

    setState(() {
      startDrag = null;
      startDragPercentScroll = null;
    });
  }

}
//********************************************************************************************************************** */