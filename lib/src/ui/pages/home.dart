import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:login_register_bloc_app/src/bloc/user_bloc.dart';
import 'package:login_register_bloc_app/src/repository/auth.dart';
import 'package:login_register_bloc_app/src/ui/pages/splash.dart';
import 'package:login_register_bloc_app/src/util/const_global.dart';
import 'package:shared_preferences/shared_preferences.dart';

//********************************** CLASS HOME PAGE ************************* */
class HomePage extends StatefulWidget {

  //************************* CONSTRUCT ************************* */
  HomePage({Key key}) : super(key: key);

  //************************** CALL STATE *********************** */
  @override
  _HomePageState createState() => _HomePageState();
}

//*********************************** STATE HOME PAGE ************************* */
class _HomePageState extends State<HomePage> {
  final blocUser =  BlocProvider.getBloc<UserBloc>();
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar( 
        
        title: Text("Home Page")

      ),

      body:Center(
        child: Container(
          color: Colors.redAccent,
          child: IconButton(
            tooltip: "Terminar sessão.",
            icon: Icon(Icons.power_settings_new, color: Colors.white,),

            //onPressed: (){}
            
            onPressed: () {
              AuthRepository.signOut().then((onValue) async { // CALL MAIN
                //Navigator.of(context).pushReplacementNamed('/login');   
                //blocUser.cha  

                SharedPreferences prefs = await SharedPreferences.getInstance();
                ConstGlobal.token = "";
                prefs.setString('token', '$ConstGlobal.token');

                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SplashPage(),
                  )
                );

              });
            },
          ),
        )  

      )   
    );
  }
}
//****************************************************************************** */