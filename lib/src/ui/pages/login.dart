import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:login_register_bloc_app/src/bloc/user_bloc.dart';
import 'package:login_register_bloc_app/src/ui/widgets/user_widget.dart'; 

import 'package:login_register_bloc_app/src/util/LanguageLocalizations.dart';
import 'package:login_register_bloc_app/src/util/socicon_icons.dart';
//import 'package:login_register_bloc_app/src/ui/widgets/sign_in_form.dart';

//************************************************ CLASS LOGIN PAGE ************************************** */
class LoginPage extends StatefulWidget {

  //*********************** CONSTRUCT *********************************
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    //bool checkLoginFinish = false;
    //final blocUser = UserBlocProvider.of(context);
    final blocUser = BlocProvider.getBloc<UserBloc>();

    return loginPage(context, blocUser);
    /*
    return Container(
      padding: EdgeInsets.all(16.0),
      decoration: BoxDecoration(
          color: Colors.amber
      ),
      alignment: Alignment(0.0,0.0),
      child: SignInForm(),
    );*/
  }

  Widget loginPage(BuildContext context, UserBloc blocUser) {
    return new Container(
      height: MediaQuery.of(context).size.height,
      //:::::::::::::::::::::::::::::::::: BACKGROUND :::::::::::::::::::::::::::::::
      decoration: BoxDecoration(
        color: Colors.white,
        image: DecorationImage(
          colorFilter: new ColorFilter.mode(
              Colors.black.withOpacity(0.05), BlendMode.dstATop),
          image: AssetImage('assets/images/mountains.jpg'),
          fit: BoxFit.cover,
        ),
      ),

      //::::::::::::::::::::::::::::: SINGLE CHIL SCROLL VIEW :::::::::::::::::::::::
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 30),
        child: SingleChildScrollView(

          //:::::::::::::::::::::::::::::::::: COLUMN ::::::::::::::::::::::::::::::::::::
          child: new Column(
            children: <Widget>[
              //--------------------------- LOGO APP --------------------------
              Container(
                padding: EdgeInsets.all(120.0),
                child: Center(
                  child: Icon(
                    Icons.headset_mic,
                    color: Colors.redAccent,
                    size: 50.0,
                  ),
                ),
              ),

              //--------------------------- TEXT EMAIL ------------------------
              UserWidgets.buildTextRow(Language.of(context).trans('email').toUpperCase()),
              //BuildWidgets.buildText("EMAIL",textAlign:TextAlign.start, sizetext: 15.0, colorText:  Colors.redAccent),

              //------------------------ TEXT FIELD EMAIL ---------------------
              UserWidgets.buildTextField(
                context, 
                blocUser, 
                Language.of(context).trans('email'), 
                "samarthagarwal@live.com"),

              //----------------------------- SEPARATE ------------------------
              Divider(
                height: 24.0,
              ),

              //--------------------------- TEXT PASSWORD ---------------------
              UserWidgets.buildTextRow(Language.of(context).trans('password').toUpperCase()),

              //------------------------ TEXT FIELD PSSWORD -------------------
              UserWidgets.buildTextField(
                context, 
                blocUser, 
                Language.of(context).trans('password'), 
                "*********"),

              //----------------------------- SEPARATE ------------------------
              Divider(
                height: 24.0,
              ),

              //----------------------- TEXT FORGOT PASSWORD ------------------
              new Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 20.0),
                    child: new FlatButton(
                      child: new Text(
                        Language.of(context).trans('forgot_password'),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.redAccent,
                          fontSize: 15.0,
                        ),
                        textAlign: TextAlign.end,
                      ),
                      onPressed: () => {},
                    ),
                  ),
                ],
              ),

              //------------------------ BUTTON LOGIN -------------------------
              //BuildWidgets.buildFlatButton(context, "LOGIN", login,
              //    Colors.redAccent, Colors.white, 20.0, blocUser: blocUser),
              UserWidgets.submitLogin(context, blocUser, Language.of(context).trans('login').toUpperCase()),

              //------------------------- SEPARATE WITH TEXT -------------------
              separateWithText(context),

              //------------------------- SOCIAL NETWORK -----------------------
              socialNetwork(context),
            ],
          ),

        )
      )    
    );
  }

  Widget loginButton (BuildContext context, UserBloc blocUser){
    //return checkLoginFinish ? LoginPage() : CircularProgressIndicator();
    return null; 
  }

  Widget separateWithText(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.only(left: 30.0, right: 30.0, top: 20.0),
      alignment: Alignment.center,
      child: Row(

        //::::::::::::::::::::::: LINE ::::::::::::::::::::::::::::
        children: <Widget>[
          new Expanded(
            child: new Container(
              margin: EdgeInsets.all(8.0),
              decoration: BoxDecoration(border: Border.all(width: 0.25)),
            ),
          ),

          //:::::::::::::::::::::: TEXT :::::::::::::::::::::::::::::
          Text(
            Language.of(context).trans('or').toUpperCase(),
            style: TextStyle(
              color: Colors.grey,
              fontWeight: FontWeight.bold,
            ),
          ),

          //::::::::::::::::::::::: LINE ::::::::::::::::::::::::::::::
          new Expanded(
            child: new Container(
              margin: EdgeInsets.all(8.0),
              decoration: BoxDecoration(border: Border.all(width: 0.25)),
            ),
          ),

        ],
      ),
    );
  }

  Widget socialNetwork(BuildContext context) {

    return Container(
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.only(left: 30.0, right: 30.0, top: 20.0),

      //:::::::::::::::::::::::::::::::::: ROW ::::::::::::::::::::::::::::::::::::::
      child: new Row(
        children: <Widget>[
          
          //::::::::::::::::::::::::: FACEBOOK ::::::::::::::::::  
          buildButtonIcon("FACEBOOK", Color(0Xff3B5998), Colors.white,  Socicon.facebook),  
           
          //::::::::::::::::::::::::: SPACE ::::::::::::::::::::: 
          SizedBox(width: 10), 

          //::::::::::::::::::::::::: GOOGLE ::::::::::::::::::::
          buildButtonIcon("GOOGLE", Color(0Xffdb3236), Colors.white,  Socicon.google), 
         
        ],
      ),
    );
  }

  Widget buildButtonIcon(String title, Color colorButton, Color titleColor, IconData icon){

    return Expanded(
      child: new Container(
        margin: EdgeInsets.only(left: 8.0),
        alignment: Alignment.center,
        
        child: new Row(
          children: <Widget>[
            new Expanded(
              child: new FlatButton(
                padding: EdgeInsets.only(top: 20.0, bottom: 20.0,),

                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0),
                ),
                      
                color: colorButton,

                onPressed: () => {},

                child: new Container(
                  child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,

                  children: <Widget>[
                    new Expanded(
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Icon(
                            //const IconData(0xe804, fontFamily: '_kFontFam'),
                            icon,
                            color: titleColor,
                            size: 15.0,
                          ),
                          
                          Text(
                            title,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: titleColor,
                              fontWeight: FontWeight.bold
                            ),
                          ),
                                
                        ],
                      ),
                    ),
                  ],
                        
                ),
              ),
            ),
          ),
        ],
      ),

    ),
    ); 
  }
}
//********************************************************************************************************** */
