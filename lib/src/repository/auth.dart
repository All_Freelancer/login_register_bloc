import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:flutter/services.dart';
import 'package:login_register_bloc_app/src/model/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

//*********************************** ENUM ***************************************** */
enum authProblems { UserNotFound, PasswordNotValid, NetworkError, UnknownError }

//*********************************** CLASS AUTH FIREBASE **************************** */
class AuthRepository {

  //**************************** SIGN IN EMAIL ************************************* */
  static Future<String> signIn(String email, String password) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String result, token; 
    try{
      //FirebaseUser user = (await FirebaseAuth.instance
      //    .signInWithEmailAndPassword(email: email, password: password)).user;
      //return user.uid;

      FirebaseUser user = (await FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: password)).user;

      token = user.uid;
      prefs.setString('token', '$token');

      result = "login";   
    }catch(e){
      result = getExceptionText(e);   
    }  

    return result; 
  }

  //***************************** SIGN IN WITH FACEBOOK **************************** */
  /*
  static Future<String> signInWithFacebok(String accessToken) async {
    FirebaseUser user = await FirebaseAuth.instance
        .signInWithFacebook(accessToken: accessToken);
    return user.uid;
  }
  */
  //***************************** SIGN UP - REGISTER ************************************ */
  static Future<String> signUp(String email, String password) async {
    String result; 
    try{
      //FirebaseUser user = (await FirebaseAuth.instance
      //  .createUserWithEmailAndPassword(email: email, password: password)).user;
      //  result = user.getIdToken().toString();   
      await FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: email, password: password); 

      result = "register"; 
    }catch(e){
      result = getExceptionText(e); 
    }
   
    //return user.uid;
    return result; 
  }

  //*********************************** SIGN OUT *********************************** */
  static Future<void> signOut() async {
    return FirebaseAuth.instance.signOut();
  }

  //*********************************** GET CURRENT USER *************************** */
  static Future<FirebaseUser> getCurrentFirebaseUser() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    return user;
  }

  //************************************** ADD USER ********************************* */
  static void addUser(UserModel user) async {
    checkUserExist(user.userID).then((value) {
      if (!value) {
        print("user ${user.firstName} ${user.email} added");
        Firestore.instance
            .document("users/${user.userID}")
            .setData(user.toJson());
      } else {
        print("user ${user.firstName} ${user.email} exists");
      }
    });
  }

  //***************************************** CHECK USER EXIT *************************** */
  static Future<bool> checkUserExist(String userID) async {
    bool exists = false;
    try {
      await Firestore.instance.document("users/$userID").get().then((doc) {
        if (doc.exists)
          exists = true;
        else
          exists = false;
      });
      return exists;
    } catch (e) {
      return false;
    }
  }

  //******************************************* GET USER ******************************* */
  static Stream<UserModel> getUser(String userID) {
    return Firestore.instance
        .collection("users")
        .where("userID", isEqualTo: userID)
        .snapshots()
        .map((QuerySnapshot snapshot) {
      return snapshot.documents.map((doc) {
        return UserModel.fromDocument(doc);
      }).first;
    });
  }

  //******************************************* EXCEPTION TEXT ***************************** */
  static String getExceptionText(Exception e) {
    if (e is PlatformException) {
      switch (e.message) {
        case 'There is no user record corresponding to this identifier. The user may have been deleted.':
          return 'User with this e-mail not found.';
          break;
        case 'The password is invalid or the user does not have a password.':
          return 'Invalid password.';
          break;
        case 'A network error (such as timeout, interrupted connection or unreachable host) has occurred.':
          return 'No internet connection.';
          break;
        case 'The email address is already in use by another account.':
          return 'Email address is already taken.';
          break;
        default:
          return 'Unknown error occured.';
      }
    } else {
      return 'Unknown error occured.';
    }
  }
}
//************************************************************************************** */
