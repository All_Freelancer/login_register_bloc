import 'package:cloud_firestore/cloud_firestore.dart';

//********************************** CLASS USER MODEL ********************************** */
class UserModel {
  final String userID;
  final String firstName;
  final String email;
  final String profilePictureURL;

  //********************************** CONSTRUCT *************************** */
  UserModel({
    this.userID,
    this.firstName,
    this.email,
    this.profilePictureURL,
  });

  //********************************* MAP TO JSON ************************** */
  Map<String, Object> toJson() {
    return {
      'userID': userID,
      'firstName': firstName,
      'email': email == null ? '' : email,
      'profilePictureURL': profilePictureURL,
      'appIdentifier': 'Login-Register-Bloc'
    };
  }

  //********************************* USER FROM JSON *********************** */
  factory UserModel.fromJson(Map<String, Object> doc) {
    UserModel user = new UserModel(
      userID: doc['userID'],
      firstName: doc['firstName'],
      email: doc['email'],
      profilePictureURL: doc['profilePictureURL'],
    );
    return user;
  }

  //******************************* CALL USER FROM DOCUMENT ****************** */
  factory UserModel.fromDocument(DocumentSnapshot doc) {
    return UserModel.fromJson(doc.data);
  }
}
//************************************************************************************* */
