import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'const_global.dart';
import 'package:shared_preferences/shared_preferences.dart';

//****************************************** CLASS LANGUAGE *************************************** */
class Language{
  //Variable
  final Locale locale;
  Map<String, String> _sentences;

  //********************************* CONSTRUCT ******************************** */
  Language(this.locale);

  
  //**************************** METHOD  */
  static Language of(BuildContext context) {
    return Localizations.of<Language>(context, Language);
  }

  //**************************** LOADING MAP LANGUAGE ************************** */
  Future<bool> load() async {
    String data = await rootBundle.loadString('assets/lang/${this.locale.languageCode}.json');
    Map<String, dynamic> _result = json.decode(data);

    this._sentences = new Map();
    _result.forEach((String key, dynamic value) {
      this._sentences[key] = value.toString();
    });

    return true;
  }

  //**********************************  GET WORD IN MAP ************************* */
  String trans(String key) {
    return this._sentences[key];
  }
}
//**************************************************************************************************** */

//************************************* CLASS DELEGATE FOR LOCALIZATIONS LANGUAGE ****************************** */
class LanguageDelegate extends LocalizationsDelegate<Language> {

  const LanguageDelegate();

  //******************************** CONTAINER LANGUAGE *********************** */
  @override
  bool isSupported(Locale locale) => ['pt', 'en'].contains(locale.languageCode);

  //******************************** LOADING LOCALIZATIONS ********************** */
  @override
  Future<Language> load(Locale locale) async {
    Language localizations = new Language(locale);
    await localizations.load();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString("language", locale.languageCode);

    ConstGlobal.codeLanguage = locale.languageCode;
   
    return localizations;
  }

  //************************************** LAST LANGUAGE *************************** */
  @override
  bool shouldReload(LanguageDelegate old) => false;
}
//*************************************************************************************************************** */