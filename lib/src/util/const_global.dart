import 'package:flutter/material.dart';

//******************************************* CLASS CONST GLOBAL **************************************** */
class ConstGlobal{

  static final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  
  static String token = "";
  
  static String codeLanguage;    //LANGUAGE 

}