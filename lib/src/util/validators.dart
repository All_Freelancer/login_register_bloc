import 'dart:async';
import 'package:flutter/widgets.dart';
import 'package:login_register_bloc_app/src/util/LanguageLocalizations.dart';

//*********************************** CLASS VALIDATION ****************************** */
class Validators {
  static BuildContext contextValidator; 


  //************************** NAME ************************ */
  static bool nameValidate(String text) {
    return text
        .contains(new RegExp(r"^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$"));
  }

  final name = StreamTransformer<String, String>.fromHandlers(
    handleData: (name, sink){
      if(nameValidate(name)){
        sink.add(name); 
      }else{
        sink.addError(Language.of(contextValidator).trans('name_validateMessage')); 
      }
    }
  ); 

  //************************** NUMBER ************************ */
  static bool numberValidate(String text) {
    Pattern pattern = r'^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$';
    RegExp regex = new RegExp(pattern);
    return regex.hasMatch(text);
  }

  final validateNumber = StreamTransformer<String, String>.fromHandlers(
    handleData: (number, sink){
      if(numberValidate(number)){
        sink.add(number);
      }else{
         sink.addError(Language.of(contextValidator).trans('number_validateMessage')); 
      }
    }
  ); 

  //************************** EMAIL ************************ */
  static bool emailValidate(String text) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return regex.hasMatch(text);
  }
  
  final validateEmail = StreamTransformer<String, String>.fromHandlers(
    handleData: (email, sink) {
      //if (email.contains('@')) {
      if (emailValidate(email)) {    
        sink.add(email);
      } else {
         sink.addError(Language.of(contextValidator).trans('email_validateMessage'));
      }
    }
  );

  //*************************** PASSWORD ********************* */
  static bool passwordValidate(String text) {
    return text.toString().length >= 6;
  }

  final validatePassword = StreamTransformer<String, String>.fromHandlers(
    handleData: (password, sink) {
      if (passwordValidate(password)) {
        sink.add(password);
      } else {
        sink.addError(Language.of(contextValidator).trans('password_validateMessage'));
      }
    }
  );

}
//************************************************************************************ */
